with open("day01.txt", 'rt') as day1:
    data = day1.read().split("\n") #new line is removed
print(data) #we can see that the new line is now substituted with " "
temp = 0
CalList = []
for i in data:
    if i != '': 
        temp += int(i) 
    
    else:
        CalList.append(temp)
        temp = 0
                         #keeps on adding the value till " " is found. it checks for " "
                         # and appends a new  value in the list if " " is found
largestvalue = (max(CalList))   #find the maximum sum in CalList
print("Largest amount of calories : " + str(largestvalue))
CalList.sort()           #Sorts the list in ascending order
sumofallthree = sum (CalList[-3:]) #Sum of last 3 values in the list and they are highest as they are in ascending order
print ("Sum of highest three: " + str(sumofallthree))